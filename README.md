# Quero ser um desenvolvedor front-end na WebJump!

Bem vindo a minha solução do desafio!

## Começo rápido
Para começar rapidamente a avalição só precisa instalar as dependências
```
npm install
```
Depois dar start na mockApi
```
npm start
```
E depois, em outro console, dar start no node. Eu usei nodemon para o desenvolvimento, então você pode executar:
```
npx nodemon index.js
```
Depois só acessar o https://localhost:3000/

## App Preview
![Preview dos breakpoint](layout/appPreview1.gif)

## Tecnologias e soluções
Para esse desafio escolhi algumas tecnologias que já sou familiar para me ajudar.

- Node.js & Express - resolvi adotar o Express para me ajudar a manipular a mockApi e me possibilitar a instalação do EJS;
- EJS - Escolhi o EJS para me ajudar a iterar sobre os jsons enviados pela mockApi;
- EJS-Mate - Usei para compor a boilerplate pra servir os tipos de conteúdo que o desafio pediu;
- Axios - Escolhido para lidar com as solicitações HTTP;
- Bootstrap - Usei alguns componentes do bootstrap pra me poupar algum tempo no CSS;
- GoogleFonts - Escolhi essa api para alcançar as fontes descritas no layout;
- FontAwesome - Usado para alguns ícones e detalhes.

## Considerações

Agradeço a oportunidade de exercitar minhas habilidades de front-end, o desafio foi muito divertido de se fazer.

Durante o desenvolvimento tive contato com aspectos do CSS que eu já não me recordava e foi muito interessante ter essa oportunidade para buscar conhecimento. 

Espero que o resultado seja satisfatório.
