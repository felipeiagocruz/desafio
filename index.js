const express = require('express');
const path = require('path');
const ejsMate = require('ejs-mate')
const axios = require('axios')

const app = express();

app.engine('ejs', ejsMate)
//Definindo o diretório de views e alterar o render para EJS
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'))


app.use(express.static(__dirname + '/public'));



//Fazendo o parse dos componentes de resposta/requerimento das funções do express:
app.use(express.urlencoded({ extended: true }))

app.get('/list/:id', async (req, res) => {
    const id = req.params.id
    let request = 'http://localhost:8888/api/V1/categories/1';
    let nome = "nome"
    switch (id) {
        case '1':
            request = 'http://localhost:8888/api/V1/categories/1';
            nome = "Camisetas"
            break
        case '2':
            request = 'http://localhost:8888/api/V1/categories/2';
            nome = "Calças"
            break
        case '3':
            request = 'http://localhost:8888/api/V1/categories/3';
            nome = "Calçados"
            break
    }

    let requestOne = await axios.get('http://localhost:8888/api/V1/categories/list')
    let requestTwo = await axios.get(request)
    const categorias = requestOne.data;
    const items = requestTwo.data;
    return res.render('list.ejs', { data: { categorias, items, nome } });



})


app.get('/', async (req, res) => {
    let categorias = []
    axios.get('http://localhost:8888/api/V1/categories/list').then(response => {
        categorias = response.data
        res.render('index.ejs', { data: { categorias } });
    })


})



app.listen(3000, function () {
    console.log('Acesse: http://localhost:3000')
});